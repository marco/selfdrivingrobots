import gym
import matplotlib.pyplot as plt
import numpy as np
import torch


ACTIONS = ['South', 'North', 'East', 'West', 'Pickup', 'Dropoff']
LOCS = ['R', 'G', 'Y', 'B', 'taxi']
MIN_VAL = 1.e-7


def render_action(action):
    """Given an int between 0 and 5, will return the corresponding action.

    For example, given `2` ("move east"), it will return 'East'.

    :param action: the action to take
    :type action: int
    :return: the corresponding action name
    :rtype: str
    """
    return ACTIONS[action]

def where_at(loc):
    """Given an int between 0 and 4, will return the corresponding location.

    For example, given `4` ("passenger in taxi"), it will return 'taxi'.

    :param loc: the location
    :type loc: int
    :return: the corresponding location name
    :rtype: str
    """
    return LOCS[loc]


def create_model(state_dim=4, actions_dim=6, mid=150, lr=0.0009):
    model = torch.nn.Sequential(
        torch.nn.Linear(state_dim, mid),
        torch.nn.LeakyReLU(),
        torch.nn.Linear(mid, actions_dim),
        torch.nn.Softmax(dim=0)
    )
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    return model, optimizer


def state2tensor(state, env):
    """Computes the tensor array of the encoded state, returned by the environment"""
    sa = np.array([i for i in env.decode(state)])
    return torch.from_numpy(sa).float()


# The policy model (a 2-layer Neural Network) computes the actions'
# probability distribution (completely random at present, as the model is
# untrained); based on that distribution, we sample the next action.
def next_action(state, model, env):
    """Uses the model to estimate the next best action, given the current state"""
    pred = model(state2tensor(state, env))
    return np.random.choice(np.arange(6), p=pred.data.numpy())


def discount_rewards(rewards, g=0.99):
    """Discounts the rewards by a gamma (g) factor, with exponential decay"""
    exps = reversed(range(len(rewards)))
    discounts = torch.pow(g, torch.tensor([x for x in exps]))

    return discounts * torch.tensor(rewards)

def loss_fn(preds, rewards, min=MIN_VAL):
    """Log-probability loss functions

    Computes the product of the discounted rewards for the log-probability of
    the action probabilities.

    As we're taking the log of `pred`, we "clip" it so that actions that
    are extremely unlikely (and would cause pred[i] to be 0) don't cause
    a NaN when taking the log()
    """
    preds = torch.clamp(preds, min, 1.0)
    return -1 * torch.sum(rewards * torch.log(preds))
